Source: openimageio
Section: libs
Priority: optional
Maintainer: Debian PhotoTools Maintainers <pkg-phototools-devel@lists.alioth.debian.org>
Uploaders: Matteo F. Vescovi <mfv@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 dh-python,
 libboost-filesystem-dev,
 libboost-python-dev,
 libboost-regex-dev,
 libboost-system-dev,
 libboost-thread-dev,
 libbz2-dev,
 libdcmtk-dev,
 libfmt-dev,
 libfreetype-dev,
 libgif-dev,
 libheif-dev,
 libjpeg-dev,
 libopencolorio-dev [!hurd-i386],
 libopencv-dev,
 libopenexr-dev,
 libopenjp2-7-dev,
 libopenvdb-dev,
 libpng-dev,
 libqt5opengl5-dev,
 libraw-dev,
 libsquish-dev,
 libssl-dev,
 libtiff-dev,
 libturbojpeg0-dev,
 libwebp-dev,
 pybind11-dev,
 python3-dev:any,
 python3-imath,
 qtbase5-dev,
 robin-map-dev (>= 0.2.0),
 txt2man
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://www.openimageio.org/
Vcs-Git: https://salsa.debian.org/debian-phototools-team/openimageio.git
Vcs-Browser: https://salsa.debian.org/debian-phototools-team/openimageio

Package: libopenimageio2.5
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Library for reading and writing images - runtime
 OpenImageIO is a library for reading and writing images, and a bunch of
 related classes, utilities, and applications. Main features include:
 .
  * Extremely simple but powerful ImageInput and ImageOutput APIs for reading
    and writing 2D images that is format agnostic -- that is, a "client app"
    doesn't need to know the details about any particular image file formats.
    Specific formats are implemented by DLL/DSO plugins.
 .
  * Format plugins for TIFF, JPEG/JFIF, OpenEXR, PNG, HDR/RGBE, Targa,
    JPEG-2000, DPX, Cineon, FITS, BMP, ICO, RMan Zfile, Softimage PIC, DDS,
    SGI, PNM/PPM/PGM/PBM, Field3d, WebP. More coming! The plugins are really
    good at understanding all the strange corners of the image formats, and
    are very careful about preserving image metadata (including Exif, GPS, and
    IPTC data).
 .
  * An ImageCache class that transparently manages a cache so that it can access
    truly vast amounts of image data (thousands of image files totaling hundreds
    of GB) very efficiently using only a tiny amount (tens of megabytes at most)
    of runtime memory. Additionally, a TextureSystem class provides filtered
    MIP-map texture lookups, atop the nice caching behavior of ImageCache.
 .
  * Several image tools based on these classes, including iinfo (print detailed
    info about images), iconvert (convert among formats, data types, or modify
    metadata), idiff (compare images), igrep (search images for matching
    metadata). Because these tools are based on ImageInput/ImageOutput, they
    work with any image formats for which ImageIO plugins are available.
 .
  * A really nice image viewer, iv, also based on OpenImageIO classes (and so
    will work with any formats for which plugins are available).
 .
  * Supported on Linux, OS X, and Windows.
 .
  * All available under the BSD license, so you may modify it and use it in both
    open source or proprietary apps.
 .
 This package contains the runtime library for building programs that use
 libOpenImageIO.

Package: libopenimageio-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libilmbase-dev,
 libopenimageio2.5 (= ${binary:Version}),
 libopenimageio-doc (>= 1.6.10~dfsg0-1),
 ${misc:Depends}
Description: Library for reading and writing images - development
 OpenImageIO is a library for reading and writing images, and a bunch of
 related classes, utilities, and applications. Main features include:
 .
  * Extremely simple but powerful ImageInput and ImageOutput APIs for reading
    and writing 2D images that is format agnostic -- that is, a "client app"
    doesn't need to know the details about any particular image file formats.
    Specific formats are implemented by DLL/DSO plugins.
 .
  * Format plugins for TIFF, JPEG/JFIF, OpenEXR, PNG, HDR/RGBE, Targa,
    JPEG-2000, DPX, Cineon, FITS, BMP, ICO, RMan Zfile, Softimage PIC, DDS,
    SGI, PNM/PPM/PGM/PBM, Field3d, WebP. More coming! The plugins are really
    good at understanding all the strange corners of the image formats, and
    are very careful about preserving image metadata (including Exif, GPS, and
    IPTC data).
 .
  * An ImageCache class that transparently manages a cache so that it can access
    truly vast amounts of image data (thousands of image files totaling hundreds
    of GB) very efficiently using only a tiny amount (tens of megabytes at most)
    of runtime memory. Additionally, a TextureSystem class provides filtered
    MIP-map texture lookups, atop the nice caching behavior of ImageCache.
 .
  * Several image tools based on these classes, including iinfo (print detailed
    info about images), iconvert (convert among formats, data types, or modify
    metadata), idiff (compare images), igrep (search images for matching
    metadata). Because these tools are based on ImageInput/ImageOutput, they
    work with any image formats for which ImageIO plugins are available.
 .
  * A really nice image viewer, iv, also based on OpenImageIO classes (and so
    will work with any formats for which plugins are available).
 .
  * Supported on Linux, OS X, and Windows.
 .
  * All available under the BSD license, so you may modify it and use it in both
    open source or proprietary apps.
 .
 This package contains the static library and headers for building programs that
 use libOpenImageIO.

Package: openimageio-tools
Section: graphics
Architecture: any
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: Library for reading and writing images - command line tools
 OpenImageIO is a library for reading and writing images, and a bunch of
 related classes, utilities, and applications. Main features include:
 .
  * Extremely simple but powerful ImageInput and ImageOutput APIs for reading
    and writing 2D images that is format agnostic -- that is, a "client app"
    doesn't need to know the details about any particular image file formats.
    Specific formats are implemented by DLL/DSO plugins.
 .
  * Format plugins for TIFF, JPEG/JFIF, OpenEXR, PNG, HDR/RGBE, Targa,
    JPEG-2000, DPX, Cineon, FITS, BMP, ICO, RMan Zfile, Softimage PIC, DDS,
    SGI, PNM/PPM/PGM/PBM, Field3d, WebP. More coming! The plugins are really
    good at understanding all the strange corners of the image formats, and
    are very careful about preserving image metadata (including Exif, GPS, and
    IPTC data).
 .
  * An ImageCache class that transparently manages a cache so that it can access
    truly vast amounts of image data (thousands of image files totaling hundreds
    of GB) very efficiently using only a tiny amount (tens of megabytes at most)
    of runtime memory. Additionally, a TextureSystem class provides filtered
    MIP-map texture lookups, atop the nice caching behavior of ImageCache.
 .
  * Several image tools based on these classes, including iinfo (print detailed
    info about images), iconvert (convert among formats, data types, or modify
    metadata), idiff (compare images), igrep (search images for matching
    metadata). Because these tools are based on ImageInput/ImageOutput, they
    work with any image formats for which ImageIO plugins are available.
 .
  * A really nice image viewer, iv, also based on OpenImageIO classes (and so
    will work with any formats for which plugins are available).
 .
  * Supported on Linux, OS X, and Windows.
 .
  * All available under the BSD license, so you may modify it and use it in both
    open source or proprietary apps.
 .
 This package contains command line tools, including iv image viewer.

Package: python3-openimageio
Section: python
Architecture: any
Multi-Arch: same
Depends: libopenimageio2.5 (= ${binary:Version}),
         python3:any,
         ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends}
Breaks: python-openimageio (<< 2.0.3~dfsg0-1)
Replaces: python-openimageio (<< 2.0.3~dfsg0-1)
Description: Library for reading and writing images - Python bindings
 OpenImageIO is a library for reading and writing images, and a bunch of
 related classes, utilities, and applications. Main features include:
 .
  * Extremely simple but powerful ImageInput and ImageOutput APIs for reading
    and writing 2D images that is format agnostic -- that is, a "client app"
    doesn't need to know the details about any particular image file formats.
    Specific formats are implemented by DLL/DSO plugins.
 .
  * Format plugins for TIFF, JPEG/JFIF, OpenEXR, PNG, HDR/RGBE, Targa,
    JPEG-2000, DPX, Cineon, FITS, BMP, ICO, RMan Zfile, Softimage PIC, DDS,
    SGI, PNM/PPM/PGM/PBM, Field3d, WebP. More coming! The plugins are really
    good at understanding all the strange corners of the image formats, and
    are very careful about preserving image metadata (including Exif, GPS, and
    IPTC data).
 .
  * An ImageCache class that transparently manages a cache so that it can access
    truly vast amounts of image data (thousands of image files totaling hundreds
    of GB) very efficiently using only a tiny amount (tens of megabytes at most)
    of runtime memory. Additionally, a TextureSystem class provides filtered
    MIP-map texture lookups, atop the nice caching behavior of ImageCache.
 .
  * Several image tools based on these classes, including iinfo (print detailed
    info about images), iconvert (convert among formats, data types, or modify
    metadata), idiff (compare images), igrep (search images for matching
    metadata). Because these tools are based on ImageInput/ImageOutput, they
    work with any image formats for which ImageIO plugins are available.
 .
  * A really nice image viewer, iv, also based on OpenImageIO classes (and so
    will work with any formats for which plugins are available).
 .
  * Supported on Linux, OS X, and Windows.
 .
  * All available under the BSD license, so you may modify it and use it in both
    open source or proprietary apps.
 .
 This package provides Python bindings to the OpenImageIO library.

Package: libopenimageio-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: libopenimageio-dev (<< 1.6.10~dfsg0-1)
Breaks: libopenimageio-dev (<< 1.6.10~dfsg0-1)
Description: Library for reading and writing images - documentation
 OpenImageIO is a library for reading and writing images, and a bunch of
 related classes, utilities, and applications. Main features include:
 .
  * Extremely simple but powerful ImageInput and ImageOutput APIs for reading
    and writing 2D images that is format agnostic -- that is, a "client app"
    doesn't need to know the details about any particular image file formats.
    Specific formats are implemented by DLL/DSO plugins.
 .
  * Format plugins for TIFF, JPEG/JFIF, OpenEXR, PNG, HDR/RGBE, Targa,
    JPEG-2000, DPX, Cineon, FITS, BMP, ICO, RMan Zfile, Softimage PIC, DDS,
    SGI, PNM/PPM/PGM/PBM, Field3d, WebP. More coming! The plugins are really
    good at understanding all the strange corners of the image formats, and
    are very careful about preserving image metadata (including Exif, GPS, and
    IPTC data).
 .
  * An ImageCache class that transparently manages a cache so that it can access
    truly vast amounts of image data (thousands of image files totaling hundreds
    of GB) very efficiently using only a tiny amount (tens of megabytes at most)
    of runtime memory. Additionally, a TextureSystem class provides filtered
    MIP-map texture lookups, atop the nice caching behavior of ImageCache.
 .
  * Several image tools based on these classes, including iinfo (print detailed
    info about images), iconvert (convert among formats, data types, or modify
    metadata), idiff (compare images), igrep (search images for matching
    metadata). Because these tools are based on ImageInput/ImageOutput, they
    work with any image formats for which ImageIO plugins are available.
 .
  * A really nice image viewer, iv, also based on OpenImageIO classes (and so
    will work with any formats for which plugins are available).
 .
  * Supported on Linux, OS X, and Windows.
 .
  * All available under the BSD license, so you may modify it and use it in both
    open source or proprietary apps.
 .
 This package provides the official documentation.
